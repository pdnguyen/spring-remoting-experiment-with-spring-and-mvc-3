package experiment.springremoting;

import java.util.List;

/**
 * User: peter.nguyen
 * Date: 22/11/12
 * Time: 10:01 PM
 */
public interface AccountService {

    public boolean insertAccount(Account account);

    public List getAccounts();

}
