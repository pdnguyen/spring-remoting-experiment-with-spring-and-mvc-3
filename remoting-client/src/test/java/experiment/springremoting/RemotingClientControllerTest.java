package experiment.springremoting;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.annotation.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.powermock.api.easymock.PowerMock.*;

/**
 * User: peter.nguyen
 * Date: 23/11/12
 * Time: 12:34 AM
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(RemotingClientController.class)
public class RemotingClientControllerTest {

    private RemotingClientController remotingClientController;

    private @Mock AccountService accountService;

    @Before
    public void setUp() throws Exception {
        remotingClientController = new RemotingClientController();
        remotingClientController.setAccountService(accountService);
    }

    @Test
    public void shouldShowAccountsPage() throws Exception {
        Model model = createMock(Model.class);
        Account existingAccount = new Account();
        Account newAccount = new Account();
        List<Account> accounts = Collections.<Account>singletonList(existingAccount);

        expect(accountService.getAccounts()).andReturn(accounts);
        expect(model.addAttribute("accounts", accounts)).andReturn(model);
        expectNew(Account.class).andReturn(newAccount);
        expect(model.addAttribute("account", newAccount)).andReturn(model);

        replayAll(model);
        String view = remotingClientController.showAccounts(model);
        verifyAll();

        assertThat(view, is("accounts"));
    }

    @Test
    public void shouldCreateAccountAndRedirectToAccountsPage() throws Exception {
        Account account = createMock(Account.class);
        Model model = createMock(Model.class);
        BindingResult bindingResult = createMock(BindingResult.class);
        HttpServletRequest httpServletRequest = createMock(HttpServletRequest.class);

        expect(accountService.insertAccount(account)).andReturn(true);

        replayAll();
        String view = remotingClientController.createAccount(account, bindingResult, model, httpServletRequest);
        verifyAll();

        assertThat(view, is("redirect:/accounts"));
    }
}
