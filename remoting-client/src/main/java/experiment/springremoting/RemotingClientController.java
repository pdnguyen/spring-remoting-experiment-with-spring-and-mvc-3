package experiment.springremoting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * User: peter.nguyen
 * Date: 22/11/12
 * Time: 10:36 PM
 */
@Controller
@RequestMapping("/accounts")
public class RemotingClientController {

    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public String showAccounts(Model model) {
        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("account", new Account());
        return "accounts";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String createAccount(@ModelAttribute("account") Account account, BindingResult result, Model model, HttpServletRequest request) {
        boolean successfullyCreated = accountService.insertAccount(account);

        return "redirect:/accounts";
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
}
